﻿using System;

namespace LSP
{
    public class Canari : Oiseau, IVolant
    {
        public void Voler()
        {
            Console.WriteLine("Je suis un canari et je vole...");
        }
        public override void Manger()
        {
            Console.WriteLine("Je suis un override et je mange...");
        }
    }
}