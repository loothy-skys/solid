﻿using System;

namespace LSP
{
    public class Autruche : Oiseau
    {
        public override void Manger()
        {
            Console.WriteLine("Je suis une autruche et je mange...");
        }
    }
}