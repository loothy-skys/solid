﻿namespace OCP
{
    public interface ILogger
    {
        void Write();
    }
}