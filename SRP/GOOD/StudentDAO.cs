﻿using System;

namespace SRP
{
    public static class StudentDAO
    {
        public static void Insert(Student student)
        {
            Console.WriteLine("Insert Student " + student);
        }

        public static void Update(Student student)
        {
            Console.WriteLine("Update Student " + student);
        }
    }
}