﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SRP.GOOD
{
    class Program
    {
        public static void Main(string[] args)
        {
            Student student = new Student();

            student.StudentId = new Guid("00000000-0000-0000-0000-000000000000");
            student.FirstName = "Alexis";
            student.LastName = "Mourlanne";
            student.BirthDay = DateTime.Now;

            StudentDAO.Insert(student);
            StudentDAO.Update(student);

            Console.WriteLine(student + " average : " + StudentUtils.CalculateAverage(student));
            Console.WriteLine(student + " graduated : " + StudentUtils.IsGraduated(student));

        }
    }
}
