﻿using System;

namespace SRP
{
    public class Student
    {
        public Guid StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDay { get; set; }

        public override string ToString()
        {
            return "(" + StudentId + ") " + FirstName + " " + LastName;
        }
    }
}