﻿namespace ISP.BAD
{
    internal interface IEnvoyer
    {
        void EnvoyerEmail(string msg);
        void EnvoyerSMS(string msg);
    }

}