﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//je n'est pas corriger le namespace quand je rentre ( ISP.GOOD dans une inteface et que je l'utillise  ISP.GOOD.IContact )il ne trouve pas les scripts dans good

namespace ISP.BAD

{
    public class Personne : ISP.BAD.IPersonne, ISP.BAD.IContact, ISP.BAD.IEnvoyer
    {
        //IPersonne
        public string Nom { get; set; }

        public string Prenom { get; set; }


        public DateTime DateDeNaissance { get; set; }

        public int Age
        {
            get
            {
                // calcul approximatif de l'age en fonction de la date de naissance
                return (int)(DateTime.Now.Subtract(DateDeNaissance).TotalDays / 365.25);
            }
        }

        //manque implementation 
        //IContact
        public int Tel { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int Mail { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        //IEnvoyer
        public void EnvoyerSMS(string msg)
        {
            // TODO
        }

        public void EnvoyerEmail(string msg)
        {
            // TODO
        }
        
       
    }
}
