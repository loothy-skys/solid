﻿using System;
namespace ISP.BAD
{
    public interface IPersonne
    {
        int Age { get; }
        DateTime DateDeNaissance { get; set; }
        string Nom { get; set; }
        string Prenom { get; set; }
      
    }
}
